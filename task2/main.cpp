#include <iostream>
#include <vector>

using namespace std;

class PhoneNumber{
private:
    string number;

public:
    string getNumber(){
        return number;
    }

    bool setNumber(const string& number){
        if(number.length() != 12 || number[0] != '+' || number[1] != '7'){
            return false;
        }
        for(size_t i = 2; i < number.length(); i++){
            if(number[i] < '0' || number[i] > '9'){
                return false;
            }
        }
        this->number = number;
        return true;
    }
};

class Abonent{
private:
    PhoneNumber number;
    string name;

public:
    void setAbonent(const PhoneNumber& number, const string& name){
        this->number = number;
        this->name = name;
    }

    string getNumber(){
        return number.getNumber();
    }

    string getName(){
        return name;
    }
};

class Phone{
private:
    vector<Abonent> phoneBook;
public:
    void addAbonent(Abonent abonent){
        phoneBook.push_back(abonent);
    }

    int findNumber(PhoneNumber number){
        int i = 0;
        for(auto abonent : phoneBook){
            if(abonent.getNumber() == number.getNumber())
                return i;
            i++;
        }
        return -1;
    }

    int findName(string name){
        int i = 0;
        for(auto abonent : phoneBook){
            if(abonent.getName() == name)
                return i;
            i++;
        }
        return -1;
    }

    void Call(int index){
        if(index >= 0 && index < phoneBook.size()){
            cout << "CALL: " << phoneBook[index].getNumber() << " "
                             << phoneBook[index].getName() << endl;
        }
    }

    void Sms(int index){
        string sms;
        if(index >= 0 && index < phoneBook.size()){
            cout << "SMS: " << phoneBook[index].getNumber() << " "
                             << phoneBook[index].getName() << endl;
            cout << "Enter message: ";
            cin >> sms;
        }
    }

    friend void PrintList(const Phone& book);
};

void PrintList(const Phone& book){
    for(auto abonent : book.phoneBook){
        cout << abonent.getNumber() << "\t" << abonent.getName() << endl;
    }
}

int main()
{
    Phone* phone = new Phone;
    string command;
    string str;
    PhoneNumber number;
    int abonentIndex;
    while (1) {
        cout << "Command: ";
        cin >> command;
        if(command == "add"){
            cout << "Enter the abonent's phone number: ";
            cin >> str;
            if(number.setNumber(str)){
                cout << "Enter the abonent's name: ";
                cin >> str;
                Abonent abonent;
                abonent.setAbonent(number, str);
                phone->addAbonent(abonent);
            }
            else{
                cout << "Wrong number!" << endl;
            }
        }
        else if(command == "call"){
            int type;
            cout << "Enter the call type (1 - number, 2 - name): ";
            cin >> type;

            if(type == 1){
                cout << "Enter the phone number: ";
                cin >> str;
                if(number.setNumber(str)){
                    abonentIndex = phone->findNumber(number);
                    if(abonentIndex >= 0){
                        phone->Call(abonentIndex);
                    }
                    else{
                        cout << "Unknown abonent." << endl;
                    }
                }
            }
            else if(type == 2){
                cout << "Enter the name: ";
                cin >> str;
                abonentIndex = phone->findName(str);
                if(abonentIndex >= 0){
                    phone->Call(abonentIndex);
                }
                else{
                    cout << "Unknown abonent." << endl;
                }
            }
            else{
                cout << "Wrong call type!";
            }
        }
        else if(command == "sms"){
            int type;
            cout << "Enter the sms type (1 - number, 2 - name): ";
            cin >> type;

            if(type == 1){
                cout << "Enter the phone number: ";
                cin >> str;
                if(number.setNumber(str)){
                    abonentIndex = phone->findNumber(number);
                    if(abonentIndex >= 0){
                        phone->Sms(abonentIndex);
                    }
                    else{
                        cout << "Unknown abonent." << endl;
                    }
                }
            }
            else if(type == 2){
                cout << "Enter the name: ";
                cin >> str;
                abonentIndex = phone->findName(str);
                if(abonentIndex >= 0){
                    phone->Sms(abonentIndex);
                }
                else{
                    cout << "Unknown abonent." << endl;
                }
            }
            else{
                cout << "Wrong sms type!";
            }
        }
        else if(command == "list"){
            PrintList(*phone);
        }
        else if(command == "exit"){
            break;
        }
        else{
            cout << "Wrong command!" << endl;
        }
    }
    delete phone;
    return 0;
}
