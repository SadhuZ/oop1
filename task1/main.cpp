#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

class Track{
private:
    string name;
    tm creationDate;
    int duration;

public:

    string getName(){
        return name;
    }
    tm getDate(){
        return creationDate;
    }
    int getDuration(){
        return duration;
    }
    void setName(string name){
        this->name = name;
    }
    void setDate(tm* date){
        creationDate = *date;
    }
    void setDuration(int duration){
        this->duration = duration;
    }
};

enum class PlayerStatus{
    PLAY,
    PAUSE,
    NEXT,
    STOP
};

class Player{
private:
    PlayerStatus status;
    vector<Track> tracks;
    int currentTrack = 0;
public:
    void AddTrack(Track track){
        tracks.push_back(track);
    }

    PlayerStatus getStatus(){
        return status;
    }

    void play(){
        if(status != PlayerStatus::PLAY){
            status = PlayerStatus::PLAY;
            cout << tracks[currentTrack].getName() << "; ";
            cout << tracks[currentTrack].getDuration() << " c.; ";
            cout << tracks[currentTrack].getDate().tm_mday << "." <<
                    tracks[currentTrack].getDate().tm_mon << "." <<
                    tracks[currentTrack].getDate().tm_year << ". " << endl;
        }
    }

    void pause(){
        if(status != PlayerStatus::PAUSE){
            status = PlayerStatus::PAUSE;
            cout << "Pause." << endl;
        }
    }

    void next(){
        if(status == PlayerStatus::PLAY){
            status = PlayerStatus::NEXT;
            srand(std::time(nullptr));
            currentTrack = tracks.size() * rand() / RAND_MAX;
            play();
        }
    }

    void stop(){
        if(status != PlayerStatus::STOP){
            status = PlayerStatus::STOP;
            cout << "Stop." << endl;
        }
    }
};

int main()
{
    Player player;

    for(int i = 0; i < 10; i++){
        string song = "Song " + to_string(i);
        int duration = 100 + i;
        tm date;
        date.tm_mday = i + 1;
        date.tm_mon = i + 1;
        date.tm_year = i + 2000;
        Track t;
        t.setName(song);
        t.setDuration(duration);
        t.setDate(&date);
        player.AddTrack(t);
    }

    string command;
    while (1) {
        cout << "Command: ";
        cin >> command;
        if(command == "play"){
            player.play();
        }
        else if(command == "pause"){
            player.pause();
        }
        else if(command == "next"){
            player.next();
        }
        else if(command == "stop"){
            player.stop();
        }
        else if(command == "exit"){
            break;
        }
        else{
            cout << "Wrong command!" << endl;
        }
    }

    return 0;
}
