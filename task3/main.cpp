#include <iostream>
#include <vector>
using namespace std;

struct Vector{
    int x;
    int y;
};

class Window{
private:
    Vector begin;
    Vector end;
public:
    Window(){
        begin.x = 0;
        begin.y = 0;
        end.x = 10;
        end.y = 10;
    }

    Vector getBegine(){
        return begin;
    }

    Vector getEnd(){
        return end;
    }

    void Move(const Vector& position){
        int deltaX = begin.x - position.x;
        int deltaY = begin.y - position.y;
        begin.x = position.x;
        begin.y = position.y;
        end.x = end.x - deltaX;
        end.y = end.y - deltaY;
    }

    void Resize(const Vector& size){
        end.x = begin.x + size.x - 1;
        end.y = begin.y + size.y - 1;
    }
};

class Monitor{
private:
    const int width = 80;
    const int height = 50;
    vector<Window> windows;

public:
    void addWindow(const Window& window){
        windows.push_back(window);
    }

    Window* getWindow(int index){
        return &windows[index];
    }

    void Display(int index){
        if(index < 0 || index >= windows.size()){
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    cout << "0";
                }
                cout << endl;
            }
        }
        else{
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    if(j >= windows[index].getBegine().x && j <= windows[index].getEnd().x){
                        if(i >= windows[index].getBegine().y && i <= windows[index].getEnd().y){
                            cout << "1";
                        }
                        else
                            cout << "0";
                    }
                    else
                        cout << "0";
                }
                cout << endl;
            }
        }
    }
};

int main()
{
    Monitor* monitor = new Monitor;
    Window* window = new Window;
    monitor->addWindow(*window);
    delete window;

    string command;
    while (1) {
        cout << "Command: ";
        cin >> command;
        if(command == "move"){
            Vector positon;
            cout << "X: ";
            cin >> positon.x;
            cout << "Y: ";
            cin >> positon.y;
            monitor->getWindow(0)->Move(positon);
            monitor->Display(0);
        }
        else if(command == "resize"){
            Vector size;
            cout << "Width: ";
            cin >> size.x;
            cout << "Height: ";
            cin >> size.y;
            monitor->getWindow(0)->Resize(size);
            monitor->Display(0);
        }
        else if(command == "display"){
            monitor->Display(0);
        }
        else if(command == "close"){
            break;
        }
        else{
            cout << "Wrong command!" << endl;
        }
    }
    delete monitor;
    return 0;
}
